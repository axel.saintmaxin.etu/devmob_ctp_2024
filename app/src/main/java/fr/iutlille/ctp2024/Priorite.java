package fr.iutlille.ctp2024;

public enum Priorite {
    FAIBLE, MOYENNE, HAUTE
}
